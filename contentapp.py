
import webapp


class contentApp (webapp.webApp):

    dicc = {'/': "<html><body><h1> Pagina principal <h1></body></html>",
                '/hola': "<html><body><h1> Hola <h1></body></html>",
                '/adios': "<html><body><h1> Adios <h1></body></html>",
                'error': "<html><body><h1> Esa ruta no esta disponible <h1></body></html>"
               }

    def parse(self, request):  # Lee lo que le llega del navegador y extrae lo util

        meth = request.split(' ', 1)[0]
        res = request.split(' ', 2)[1]
        body = request.split('\n', 2)[-1]

        return meth, res, body

    def process(self, parsedRequest):  # Coge los datos que ha sacado parse y lo utiliza

        meth, res, body = parsedRequest

        if meth == "GET":
            httpCode = "200 OK"
            if res in self.dicc.keys():
                htmlBody = self.dicc[res]
            else:
                htmlBody = self.dicc["error"]

            return httpCode, htmlBody


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
